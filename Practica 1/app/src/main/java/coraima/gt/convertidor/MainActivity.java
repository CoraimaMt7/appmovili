package coraima.gt.convertidor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText edit;
    ToggleButton tbtn;
    Double a;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edit=(EditText) findViewById(R.id.editText);
        btn=(Button) findViewById(R.id.button);
        tbtn=(ToggleButton) findViewById(R.id.toggleButton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit.getText().toString().isEmpty())
                {
                    Toast.makeText(MainActivity.this,"Ingrese la Temperatura",Toast.LENGTH_SHORT).show();
                }
                else if(tbtn.isChecked())
                {
                    a=Double.parseDouble(String.valueOf(edit.getText()));
                    Double b=a*9/5+32;
                    String r=String.valueOf(b);
                    Toast.makeText(MainActivity.this,r+" °F",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    a=Double.parseDouble(String.valueOf(edit.getText()));
                    Double b=a-32;
                    Double c=b*5/9;
                    String r=String.valueOf(c);
                    Toast.makeText(MainActivity.this,r+" °C",Toast.LENGTH_SHORT).show();
                }
            }
        });
        Log.i("Aplicación Móvil I", "Mera Torres Coraima");
    }
}
//Log: (métodos) o alternativas para poder imprimir en consola.